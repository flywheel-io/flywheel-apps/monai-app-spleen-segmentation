FROM nvcr.io/nvidia/pytorch:21.11-py3 as base

ENV FLYWHEEL="/flywheel/v0"
WORKDIR ${FLYWHEEL}

RUN apt-get update &&  \
    apt-get install --no-install-recommends -y git && \
    apt-get clean && \
    rm -rf /var/lib/apt/lists/* /tmp/* /var/tmp/*

# Installing main dependencies
COPY requirements.txt $FLYWHEEL/
RUN pip install --no-cache-dir -r $FLYWHEEL/requirements.txt

# Installing the current files
COPY run.py manifest.json $FLYWHEEL/
COPY fw_gear_monai_app_spleen_segmentation $FLYWHEEL/fw_gear_monai_app_spleen_segmentation
COPY models $FLYWHEEL/models

# Configure entrypoint
RUN chmod a+x $FLYWHEEL/run.py
ENTRYPOINT ["python","/flywheel/v0/run.py"]
