#!/usr/bin/env python
"""The run script"""
import logging
import sys
from pathlib import Path

from flywheel_gear_toolkit import GearToolkitContext

from fw_gear_monai_app_spleen_segmentation.app.app import AISpleenSegApp

MODELS_DIR = Path(__file__).parent / "models"

log = logging.getLogger(__name__)


def main(context: GearToolkitContext) -> None:  # pragma: no cover
    """Parses config and run"""
    log.info("This is the beginning of the run file")
    app = AISpleenSegApp()
    app.run(
        input=context.get_input_path("dicom-file"),
        output=str(context.output_dir),
        model=str(MODELS_DIR / "model.ts"),
    )
    sys.exit(0)


if __name__ == "__main__":  # pragma: no cover
    with GearToolkitContext() as context:
        context.init_logging()
        main(context)
