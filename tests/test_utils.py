"""Module to test main.py"""

import shutil
from argparse import Namespace
from pathlib import Path
from unittest.mock import MagicMock, patch

from monai.deploy.core import ExecutionContext, InputContext, OutputContext

from fw_gear_monai_app_spleen_segmentation.utils import UnzipOperator


@patch.object(InputContext, "get")
def test_unzip_operator(mock_get, tmpdir):
    Path(tmpdir).touch()
    shutil.make_archive(tmpdir / "archive", "zip", tmpdir)
    exc_context = MagicMock(spec=ExecutionContext)
    mock_get.return_value = Namespace(path=str(tmpdir / "archive.zip"))
    in_context = InputContext(exc_context)
    out_context = MagicMock(spec=OutputContext)

    op = UnzipOperator()
    op.compute(in_context, out_context, exc_context)

    out_context.set.assert_called_once()
