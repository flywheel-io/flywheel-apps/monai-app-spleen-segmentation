from fw_gear_monai_app_spleen_segmentation.app.app import AISpleenSegApp

if __name__ == "__main__":
    AISpleenSegApp(do_run=True)
