# App source

This app is sourced from MONAI deploy app SDK directly.
The source code can be found at [here](https://github.com/Project-MONAI/monai-deploy-app-sdk/tree/main/examples/apps/ai_spleen_seg_app).
