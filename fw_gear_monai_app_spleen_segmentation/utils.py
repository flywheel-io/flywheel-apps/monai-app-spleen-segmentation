from pathlib import Path
from zipfile import ZipFile

import monai.deploy.core as md
from monai.deploy.core import (
    DataPath,
    ExecutionContext,
    InputContext,
    IOType,
    Operator,
    OutputContext,
)


@md.input("zip_archive", DataPath, IOType.DISK)
@md.output("unzip_folder", DataPath, IOType.DISK)
class UnzipOperator(Operator):
    """This operator unzip a zip archive."""

    def compute(
        self,
        op_input: InputContext,
        op_output: OutputContext,
        context: ExecutionContext,
    ):
        """Performs computation for this operator and handlesI/O."""

        input_path = Path(op_input.get().path)
        if ZipFile(input_path).testzip() is not None:
            raise ValueError("input_path is corrupted or is not a zip file.")
        unzip_folder = input_path.parent / input_path.stem
        with ZipFile(input_path, "r") as zip_file:
            zip_file.extractall(unzip_folder)

        op_output.set(DataPath(unzip_folder), "unzip_folder")
