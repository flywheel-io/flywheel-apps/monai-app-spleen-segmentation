# MONAI Spleen Segmentation App

## Summary

Run MONAI CT Spleen Segmentation application as defined in MONAI Deploy App SDK tutorials
[here](https://github.com/Project-MONAI/monai-deploy-app-sdk/tree/main/examples/apps/ai_spleen_seg_app).

## Usage

Takes as input a DICOM archive of a CT scan of the spleen and outputs a segmentation of
the spleen.

### Inputs

* __dicom-input__: A DICOM archive of a CT scan of the spleen.

### Configuration

* __debug__ (boolean, default False): Include debug statements in output.

## Contributing

For more information about how to get started contributing to that gear,
checkout [CONTRIBUTING.md](CONTRIBUTING.md).
